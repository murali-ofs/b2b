package com.b2b.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
	
import com.b2b.account.domain.DD_B2B_CUSTOMER;
import com.b2b.account.service.B2BCustomerService;

@RestController
public class AccountController {

	@Autowired
	private B2BCustomerService service;
	
	@PostMapping(value = "/sign_in")
	public ResponseEntity<DD_B2B_CUSTOMER> signIn(@RequestBody DD_B2B_CUSTOMER ddB2BCustomer) {
		return new ResponseEntity<DD_B2B_CUSTOMER>(ddB2BCustomer, HttpStatus.OK);
	}

	@PostMapping(value = "/user_sign_in")
	public ResponseEntity<DD_B2B_CUSTOMER> usersignin(@RequestBody DD_B2B_CUSTOMER ddB2BCustomer) {
		DD_B2B_CUSTOMER b2bCustomer = service.signIn(ddB2BCustomer);
		if (b2bCustomer==null) 	return new ResponseEntity<DD_B2B_CUSTOMER>(b2bCustomer, HttpStatus.NOT_FOUND);
		return new ResponseEntity<DD_B2B_CUSTOMER>(b2bCustomer, HttpStatus.OK);
	}

	@PostMapping(path = "/sign_up")
	public DD_B2B_CUSTOMER createUser(@RequestBody DD_B2B_CUSTOMER ddB2BCustomer) {
		System.out.println("request**********************"+ddB2BCustomer.getCstEmail());
		DD_B2B_CUSTOMER b2bCustomer = service.signUp(ddB2BCustomer);
		return b2bCustomer;
	}
	
	@GetMapping(path = "/get_user/{id}")
	public DD_B2B_CUSTOMER getAccountByName(@PathVariable Integer id) {
		return service.getUserbyId(id);
	}

//	@RequestMapping(path = "/")
//	@PostMapping
//	public Account createNewAccount(@Valid  User user) {
//		return accountService.create(user);
//	}

}
