package com.b2b.account.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2b.account.domain.DD_B2B_CUSTOMER;
import com.b2b.account.repository.B2BCustomerRepository;

@Service
@Transactional
public class B2BCustomerService {
	@Autowired
	B2BCustomerRepository b2bCustomerRepository;

	public DD_B2B_CUSTOMER signUp(DD_B2B_CUSTOMER user) {
		b2bCustomerRepository.save(user);
		return user;
	}
	
	public DD_B2B_CUSTOMER getUserbyId(Integer id) {
 		return b2bCustomerRepository.findById(id).get();
	}
	
	public DD_B2B_CUSTOMER signIn(DD_B2B_CUSTOMER ddb2bCustomer) {
		DD_B2B_CUSTOMER userIns= b2bCustomerRepository.signIn(ddb2bCustomer.getCstEmail(),ddb2bCustomer.getCstPassword());
		return userIns;
		
	}
}
