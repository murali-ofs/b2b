package com.b2b.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b2b.account.domain.DD_B2B_CUSTOMER;


@Repository
public interface B2BCustomerRepository extends JpaRepository<DD_B2B_CUSTOMER, Integer> {

    @Query("SELECT b2b FROM DD_B2B_CUSTOMER b2b WHERE b2b.cstEmail = :email and b2b.cstPassword = :pwd")
    public DD_B2B_CUSTOMER signIn(@Param("email") String email,@Param("pwd")String pwd);
}
