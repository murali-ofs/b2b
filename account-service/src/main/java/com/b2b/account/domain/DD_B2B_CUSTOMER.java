package com.b2b.account.domain;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

 
@Entity
@Table(name="DD_B2B_CUSTOMER")
public class DD_B2B_CUSTOMER implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "CST_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer cstId;
	
	@Column(name = "CST_EMAIL")
	private String cstEmail;
	
	@Column(name = "CST_PASSWORD")
	private String cstPassword;

	public Integer getCstId() {
		return cstId;
	}

	public void setCstId(Integer cstId) {
		this.cstId = cstId;
	}

	public String getCstEmail() {
		return cstEmail;
	}

	public void setCstEmail(String cstEmail) {
		this.cstEmail = cstEmail;
	}

	public String getCstPassword() {
		return cstPassword;
	}

	public void setCstPassword(String cstPassword) {
		this.cstPassword = cstPassword;
	}
	
 
 	
	
 
}
