package com.b2b.notification.service;

public interface NotificationService {

	void sendBackupNotifications();

	void sendRemindNotifications();
}
