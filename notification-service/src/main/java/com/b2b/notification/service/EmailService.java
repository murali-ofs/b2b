package com.b2b.notification.service;

import javax.mail.MessagingException;

import com.b2b.notification.domain.NotificationType;
import com.b2b.notification.domain.Recipient;

import java.io.IOException;

public interface EmailService {

	void send(NotificationType type, Recipient recipient, String attachment) throws MessagingException, IOException;

}
