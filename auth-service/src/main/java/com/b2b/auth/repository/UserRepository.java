package com.b2b.auth.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.b2b.auth.domain.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

}
