package com.b2b.auth.service;

import com.b2b.auth.domain.User;

public interface UserService {

	void create(User user);

}
